<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/index', function () {
    return view('index');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/contact-us', function () {
    return view('contact-us');
});

Route::get('/properties-full-grid-1', function () {
    return view('properties-full-grid-1');
});

Route::get('/single-property-3', function () {
    return view('single-property-3');
});

Route::get('/agencies-listing-1', function () {
    return view('agencies-listing-1');
});

Route::get('/agencies-details', function () {
    return view('agencies-details');
});
