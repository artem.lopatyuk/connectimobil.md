<!-- Header Container
================================================== -->
<header id="header-container" @yield('name_page')>
    <!-- Header -->
    <div id="header" class="header head-tr bottom">
        <div class="container container-header">
            <!-- Left Side Content -->
            <div class="left-side">
                <!-- Logo -->
                <div id="logo">
                    <a href="index"><img @yield('logo_header') src="images/logo-red.svg" data-sticky-logo="images/logo-light-dark.svg" alt=""></a>
                </div>
                <!-- Mobile Navigation -->
                <div class="mmenu-trigger">
                    <button class="hamburger hamburger--collapse" type="button">
                                <span class="hamburger-box">
							<span class="hamburger-inner"></span>
                                </span>
                    </button>
                </div>
                <!-- Main Navigation -->
                <nav id="navigation" @yield('style_header_nav') class="style-1">
                    <ul id="responsive">
                        <li><a href="#">Аренда</a>
                            <ul>
                                <li><a href="properties-full-grid-1">Квартиры</a></li>
                                <li><a href="properties-full-grid-1">Дома</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Продажа</a>
                            <ul>
                                <li><a href="properties-full-grid-1">Квартиры</a></li>
                                <li><a href="properties-full-grid-1">Дома</a></li>
                            </ul>
                        </li>
                        <li><a href="about">О нас</a></li>
                        <li><a href=agencies-listing-1>Команда</a></li>
                        <li><a href="contact-us">Контакты</a></li>
                    </ul>
                </nav>
                <!-- Main Navigation / End -->
            </div>
            <!-- Left Side Content / End -->

            <!-- Right Side Content / End -->
            <div class="right-side">
                <!-- Header Widget -->
                <div class="header-widget">
                    <a href="tel:@lang('+37366996699')" class="button border"><nobr>+37366996699</nobr></a>
                </div>
                <!-- Header Widget / End -->
            </div>
            <!-- Right Side Content / End -->

            <!-- lang-wrap-->
            <div class="header-user-menu user-menu add d-none d-lg-none d-xl-flex">
                <div class="lang-wrap">
                    <div class="show-lang"><span><i
                                class="fas fa-globe-americas"></i><strong>RUS</strong></span><i
                            class="fa fa-caret-down arrlan"></i></div>
                    <ul class="lang-tooltip lang-action no-list-style">
                        <li><a href="#" class="current-lan" data-lantext="Ru">Russian</a></li>
                        <li><a href="#" data-lantext="Ro">Română</a></li>
                    </ul>
                </div>
            </div>
            <!-- lang-wrap end-->

        </div>
    </div>
    <!-- Header / End -->

</header>
<div class="clearfix"></div>
<!-- Header Container / End -->
