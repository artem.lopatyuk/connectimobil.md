@extends('layout')

@section('title')
    Main - Connectimobil.md
@endsection

@section('name_page')
    class="header head-tr"
@endsection

@section('style_header_nav')
    class="style-1 head-tr"
@endsection

@section('logo_header')
    src="images/logo-white-1.svg"
@endsection

@section('body')
    <div class="int_white_bg h19 homepage-5 homepage-19">
        <!-- Wrapper -->
        <div id="wrapper" class="int_main_wraapper">
            @endsection

            @section('/body')
        </div>
        <!-- Wrapper / End -->
    </div>
@endsection

@section('main_content')

    <!-- SLIDER START (Блок «Некоторые объекты» или «Объекты по горячей цене») -->
    <div id="rev_slider_26_1_wrapper " class="rev_slider_wrapper fullscreen-container home-rev-slider"
         data-alias="mask-showcase" data-source="gallery">
        <!-- START REVOLUTION SLIDER 5.4.1 fullscreen mode -->
        <div id="rev_slider_26_1" class="rev_slider fullscreenbanner" style="display:none;"
             data-version="5.4.1">
            <ul>

                <!-- SLIDE 1 -->
                <li data-index="rs-73" data-transition="fade" data-slotamount="default"
                    data-hideafterloop="0"
                    data-hideslideonmobile="off" data-easein="default" data-easeout="default"
                    data-masterspeed="300"
                    data-thumb="images/slider/p-1.png" data-rotate="0" data-saveperformance="off"
                    data-title=""
                    data-param1="1" data-param2="" data-param3="" data-param4="" data-param5=""
                    data-param6=""
                    data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="images/slider/p-1.png" data-bgcolor='#f8f8f8' style='' alt=""
                         data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat"
                         data-bgparallax="off" class="rev-slidebg" data-no-retina>

                    <!-- LAYER 1  right image overlay dark-->
                    <div class="tp-caption tp-shape tp-shapewrapper  rs-parallaxlevel-tobggroup"
                         id="slide-73-layer-1" data-x="['right','right','right','center']"
                         data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']"
                         data-voffset="['0','0','0','0']" data-fontweight="['100','100','400','400']"
                         data-width="['full','full','full','full']"
                         data-height="['full','full','full','full']"
                         data-whitespace="nowrap" data-type="shape" data-basealign="slide"
                         data-responsive_offset="off" data-responsive="off"
                         data-frames='[{"from":"opacity:0;","speed":1500,"to":"o:0;","delay":150,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1500,"to":"opacity:0;","ease":"Power2.easeInOut"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 6;text-transform:left;background-color:rgba(0,0,0,0.5);">
                    </div>

                    <!-- LAYERS 2 number block-->
                    <div class="tp-caption rev-btn  tp-resizeme slider-block sx-bg-primary"
                         id="slide-73-layer-2"
                         data-x="['left','left','left','center']" data-hoffset="['60','60','30','0']"
                         data-y="['middle','middle','middle','top']"
                         data-voffset="['-220','-220','-220','50']"
                         data-fontweight="['600','600','600','600']" data-fontsize="['120','120','80','80']"
                         data-lineheight="['120','120','80','80']" data-height="none"
                         data-whitespace="nowrap"
                         data-type="button" data-responsive_offset="on" data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},
                                {"delay":"wait","speed":500,"to":"y:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power1.easeIn"}]'
                         data-textAlign="['left','left','left','center']" data-paddingtop="[20,20,20,20]"
                         data-paddingright="[10,10,10,10]" data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[10,10,10,10]"
                         style="z-index: 10; font-family: 'Poppins', sans-serif;">
                        01
                    </div>

                    <!-- LAYER 3  Thin text title-->
                    <div class="tp-caption   tp-resizeme slider-tag-line" id="slide-74-layer-3"
                         data-x="['left','left','left','center']" data-hoffset="['60','60','30','0']"
                         data-y="['middle','middle','middle','top']"
                         data-voffset="['-80','-80','-80','170']"
                         data-fontsize="['64','64','60','40']" data-lineheight="['74','74','70','50']"
                         data-width="['700','650','620','380']" data-height="none" data-whitespace="nowrap"
                         data-type="text" data-responsive_offset="on" data-frames='[{"delay":300,"speed":750,"sfxcolor":"#fff","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},
                                {"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoleft","frame":"999","to":"z:0;","ease":"Power4.easeOut"}]'
                         data-textAlign="['left','left','left','center']" data-paddingtop="[10,10,10,10]"
                         data-paddingright="[20,20,20,0]" data-paddingbottom="[10,10,10,10]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 10; font-weight:200; letter-spacing:10px; color: #fff;font-family: 'Poppins', sans-serif; text-transform:uppercase">
                        Elegant
                    </div>

                    <!-- LAYER 4  Bold Title-->
                    <div class="tp-caption   tp-resizeme" id="slide-74-layer-4"
                         data-x="['left','left','left','center']" data-hoffset="['60','60','30','0']"
                         data-y="['middle','middle','middle','top']" data-voffset="['10','10','10','230']"
                         data-fontsize="['64','64','60','40']" data-lineheight="['74','74','70','50']"
                         data-width="['700','700','700','700']" data-height="none" data-whitespace="normal"
                         data-type="text" data-responsive_offset="on" data-frames='[{"delay":200,"speed":750,"sfxcolor":"#fff","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},
                                {"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoleft","frame":"999","to":"z:0;","ease":"Power4.easeOut"}]'
                         data-textAlign="['left','left','left','center']" data-paddingtop="[20,20,20,20]"
                         data-paddingright="[20,20,20,10]" data-paddingbottom="[30,30,30,30]"
                         data-paddingleft="[0,0,0,10]"
                         style="z-index: 10; text-transform:uppercase; letter-spacing:10px; white-space: normal;font-weight: 600; color: #fff;font-family: 'Poppins', sans-serif;">
                        Luxury House
                    </div>

                    <!-- LAYER 5  Paragraph-->
                    <div class="tp-caption   tp-resizeme" id="slide-74-layer-5"
                         data-x="['left','left','left','center']" data-hoffset="['60','60','30','0']"
                         data-y="['middle','middle','middle','top']" data-voffset="['90','90','90','300']"
                         data-fontsize="['20','20','20','20']" data-lineheight="['30','30','30','30']"
                         data-width="['600','600','600','380']" data-height="none" data-whitespace="normal"
                         data-type="text" data-responsive_offset="on" data-frames='[{"delay":200,"speed":750,"sfxcolor":"#fff","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},
                                {"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoleft","frame":"999","to":"z:0;","ease":"Power4.easeOut"}]'
                         data-textAlign="['left','left','left','center']" data-paddingtop="[20,20,20,20]"
                         data-paddingright="[20,20,20,20]" data-paddingbottom="[30,30,30,30]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 10; white-space: normal; color: #fff;font-family: 'Poppins', sans-serif;">
                        Interior designing is the art and science which enhance your mind.
                    </div>

                    <!-- LAYER 6  Read More-->
                    <div class="tp-caption rev-btn  tp-resizeme" id="slide-74-layer-6"
                         data-x="['left','left','left','center']" data-hoffset="['60','60','30','0']"
                         data-y="['middle','middle','middle','top']"
                         data-voffset="['180','180','180','410']"
                         data-width="none" data-height="none" data-whitespace="nowrap" data-type="button"
                         data-responsive_offset="on" data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},
                                {"delay":"wait","speed":500,"to":"y:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power1.easeIn"}]'
                         data-textAlign="['left','left','left','center']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index:9; line-height:30px;"><a href="Javascript:;"
                                                                 class="site-button-secondry btn-half"><span> Read More</span></a>
                    </div>

                    <!-- LAYER 7 left dark Block -->
                    <div class="tp-caption rev-btn  tp-resizeme rev-slider-white-block"
                         id="slide-74-layer-7"
                         data-x="['right','right','left','right']" data-hoffset="['870','570','0','870']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-width="full" data-height="full" data-whitespace="nowrap" data-type="button"
                         data-responsive_offset="on" data-frames='[{"from":"y:[-0%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":0,"to":"o:1;","delay":0,"ease":"Power3.easeInOut"},
                                {"delay":"wait","speed":0,"to":"y:[-0%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power1.easeIn"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[250,250,250,250]"
                         data-paddingright="[250,150,150,150]" data-paddingbottom="[250,250,250,250]"
                         data-paddingleft="[250,150,250,250]"
                         style="z-index: 6; width:6000px;background-color:#687389 ;height:100vh;"></div>

                    <!-- Border left Part -->
                    <div class="tp-caption tp-shape tp-shapewrapper " id="slide-74-layer-8"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-width="full" data-height="full" data-whitespace="nowrap"
                         data-visibility="['on','on','off','off']" data-type="shape" data-basealign="slide"
                         data-responsive_offset="off" data-responsive="off"
                         data-frames='[{"delay":50,"speed":100,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power3.easeIn"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index:8;background-color:rgba(0, 0, 0, 0);border-left:40px solid #eef1f2;"></div>

                    <!-- Border bottom Part -->
                    <div class="tp-caption tp-shape tp-shapewrapper " id="slide-74-layer-7"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-width="full" data-height="full" data-whitespace="nowrap"
                         data-visibility="['on','on','off','off']" data-type="shape" data-basealign="slide"
                         data-responsive_offset="off" data-responsive="off"
                         data-frames='[{"delay":50,"speed":100,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power3.easeIn"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index:8;background-color:rgba(0, 0, 0, 0);border-bottom:80px solid #eef1f2;"></div>
                </li>

                <!-- SLIDE 2  -->
                <li data-index="rs-74" data-transition="fade" data-slotamount="default"
                    data-hideafterloop="0"
                    data-hideslideonmobile="off" data-easein="default" data-easeout="default"
                    data-masterspeed="300"
                    data-thumb="images/slider/p-2.png" data-rotate="0" data-saveperformance="off"
                    data-title=""
                    data-param1="1" data-param2="" data-param3="" data-param4="" data-param5=""
                    data-param6=""
                    data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="images/slider/p-2.png" data-bgcolor='#f8f8f8' style='' alt=""
                         data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat"
                         data-bgparallax="off" class="rev-slidebg" data-no-retina>

                    <!-- LAYER 1  right image overlay dark-->
                    <div class="tp-caption tp-shape tp-shapewrapper  rs-parallaxlevel-tobggroup"
                         id="slide-74-layer-1" data-x="['right','right','right','center']"
                         data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']"
                         data-voffset="['0','0','0','0']" data-fontweight="['100','100','400','400']"
                         data-width="['full','full','full','full']"
                         data-height="['full','full','full','full']"
                         data-whitespace="nowrap" data-type="shape" data-basealign="slide"
                         data-responsive_offset="off" data-responsive="off"
                         data-frames='[{"from":"opacity:0;","speed":1500,"to":"o:0;","delay":150,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1500,"to":"opacity:0;","ease":"Power2.easeInOut"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 6;text-transform:left;background-color:rgba(0,0,0,0.5);">
                    </div>

                    <!-- LAYERS 2 number block-->
                    <div class="tp-caption rev-btn  tp-resizeme slider-block sx-bg-primary"
                         id="slide-74-layer-2"
                         data-x="['left','left','left','center']" data-hoffset="['60','60','30','0']"
                         data-y="['middle','middle','middle','top']"
                         data-voffset="['-220','-220','-220','50']"
                         data-fontweight="['600','600','600','600']" data-fontsize="['120','120','80','80']"
                         data-lineheight="['120','120','80','80']" data-height="none"
                         data-whitespace="nowrap"
                         data-type="button" data-responsive_offset="on" data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},
                                {"delay":"wait","speed":500,"to":"y:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power1.easeIn"}]'
                         data-textAlign="['left','left','left','center']" data-paddingtop="[20,20,20,20]"
                         data-paddingright="[10,10,10,10]" data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[10,10,10,10]"
                         style="z-index: 10; font-family: 'Poppins', sans-serif;">
                        02
                    </div>


                    <!-- LAYER 3  Thin text title-->
                    <div class="tp-caption   tp-resizeme slider-tag-line" id="slide-74-layer-3"
                         data-x="['left','left','left','center']" data-hoffset="['60','60','30','0']"
                         data-y="['middle','middle','middle','top']"
                         data-voffset="['-80','-80','-80','170']"
                         data-fontsize="['64','64','60','40']" data-lineheight="['74','74','70','50']"
                         data-width="['700','650','620','380']" data-height="none" data-whitespace="nowrap"
                         data-type="text" data-responsive_offset="on" data-frames='[{"delay":300,"speed":750,"sfxcolor":"#fff","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},
                                {"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoleft","frame":"999","to":"z:0;","ease":"Power4.easeOut"}]'
                         data-textAlign="['left','left','left','center']" data-paddingtop="[10,10,10,10]"
                         data-paddingright="[20,20,20,0]" data-paddingbottom="[10,10,10,10]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 10; font-weight:200; letter-spacing:10px; color: #fff;font-family: 'Poppins', sans-serif; text-transform:uppercase">
                        Elegant
                    </div>

                    <!-- LAYER 4  Bold Title-->
                    <div class="tp-caption   tp-resizeme" id="slide-74-layer-4"
                         data-x="['left','left','left','center']" data-hoffset="['60','60','30','0']"
                         data-y="['middle','middle','middle','top']" data-voffset="['10','10','10','230']"
                         data-fontsize="['64','64','60','40']" data-lineheight="['74','74','70','50']"
                         data-width="['700','700','700','700']" data-height="none" data-whitespace="normal"
                         data-type="text" data-responsive_offset="on" data-frames='[{"delay":200,"speed":750,"sfxcolor":"#fff","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},
                                {"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoleft","frame":"999","to":"z:0;","ease":"Power4.easeOut"}]'
                         data-textAlign="['left','left','left','center']" data-paddingtop="[20,20,20,20]"
                         data-paddingright="[20,20,20,10]" data-paddingbottom="[30,30,30,30]"
                         data-paddingleft="[0,0,0,10]"
                         style="z-index: 10; text-transform:uppercase; letter-spacing:10px; white-space: normal;font-weight: 600; color: #fff;font-family: 'Poppins', sans-serif;">
                        Luxury House
                    </div>

                    <!-- LAYER 5  Paragraph-->
                    <div class="tp-caption   tp-resizeme" id="slide-74-layer-5"
                         data-x="['left','left','left','center']" data-hoffset="['60','60','30','0']"
                         data-y="['middle','middle','middle','top']" data-voffset="['90','90','90','300']"
                         data-fontsize="['20','20','20','20']" data-lineheight="['30','30','30','30']"
                         data-width="['600','600','600','380']" data-height="none" data-whitespace="normal"
                         data-type="text" data-responsive_offset="on" data-frames='[{"delay":200,"speed":750,"sfxcolor":"#fff","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},
                                {"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoleft","frame":"999","to":"z:0;","ease":"Power4.easeOut"}]'
                         data-textAlign="['left','left','left','center']" data-paddingtop="[20,20,20,20]"
                         data-paddingright="[20,20,20,20]" data-paddingbottom="[30,30,30,30]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 10; white-space: normal; color: #fff;font-family: 'Poppins', sans-serif;">
                        Interior designing is the art and science which enhance your mind.
                    </div>

                    <!-- LAYER 6  Read More-->
                    <div class="tp-caption rev-btn  tp-resizeme" id="slide-74-layer-6"
                         data-x="['left','left','left','center']" data-hoffset="['60','60','30','0']"
                         data-y="['middle','middle','middle','top']"
                         data-voffset="['180','180','180','410']"
                         data-width="none" data-height="none" data-whitespace="nowrap" data-type="button"
                         data-responsive_offset="on" data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},
                                {"delay":"wait","speed":500,"to":"y:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power1.easeIn"}]'
                         data-textAlign="['left','left','left','center']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index:9; line-height:30px;"><a href="Javascript:;"
                                                                 class="site-button-secondry btn-half"><span> Read More</span></a>
                    </div>

                    <!-- LAYER 7 left dark Block -->
                    <div class="tp-caption rev-btn  tp-resizeme rev-slider-white-block"
                         id="slide-74-layer-7"
                         data-x="['right','right','left','right']" data-hoffset="['870','570','0','870']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-width="full" data-height="full" data-whitespace="nowrap" data-type="button"
                         data-responsive_offset="on" data-frames='[{"from":"y:[-0%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":0,"to":"o:1;","delay":0,"ease":"Power3.easeInOut"},
                                {"delay":"wait","speed":0,"to":"y:[-0%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power1.easeIn"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[250,250,250,250]"
                         data-paddingright="[250,150,150,150]" data-paddingbottom="[250,250,250,250]"
                         data-paddingleft="[250,150,250,250]"
                         style="z-index: 6; width:6000px;background-color:#64827C ;height:100vh;"></div>

                    <!-- Border left Part -->
                    <div class="tp-caption tp-shape tp-shapewrapper " id="slide-74-layer-8"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-width="full" data-height="full" data-whitespace="nowrap"
                         data-visibility="['on','on','off','off']" data-type="shape" data-basealign="slide"
                         data-responsive_offset="off" data-responsive="off"
                         data-frames='[{"delay":50,"speed":100,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power3.easeIn"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index:8;background-color:rgba(100, 130, 124, 0);border-left:40px solid #eef1f2;"></div>

                    <!-- Border bottom Part -->
                    <div class="tp-caption tp-shape tp-shapewrapper " id="slide-74-layer-7"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-width="full" data-height="full" data-whitespace="nowrap"
                         data-visibility="['on','on','off','off']" data-type="shape" data-basealign="slide"
                         data-responsive_offset="off" data-responsive="off"
                         data-frames='[{"delay":50,"speed":100,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power3.easeIn"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index:8;background-color:rgba(0, 0, 0, 0);border-bottom:80px solid #eef1f2;"></div>
                </li>

                <!-- SLIDE 3 -->
                <li data-index="rs-75" data-transition="fade" data-slotamount="default"
                    data-hideafterloop="0"
                    data-hideslideonmobile="off" data-easein="default" data-easeout="default"
                    data-masterspeed="300"
                    data-thumb="images/slider/p-3.png" data-rotate="0" data-saveperformance="off"
                    data-title=""
                    data-param1="1" data-param2="" data-param3="" data-param4="" data-param5=""
                    data-param6=""
                    data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="images/slider/p-3.png" data-bgcolor='#f8f8f8' style='' alt=""
                         data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat"
                         data-bgparallax="off" class="rev-slidebg" data-no-retina>

                    <!-- LAYER 1  right image overlay dark-->
                    <div class="tp-caption tp-shape tp-shapewrapper  rs-parallaxlevel-tobggroup"
                         id="slide-75-layer-1" data-x="['right','right','right','center']"
                         data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']"
                         data-voffset="['0','0','0','0']" data-fontweight="['100','100','400','400']"
                         data-width="['full','full','full','full']"
                         data-height="['full','full','full','full']"
                         data-whitespace="nowrap" data-type="shape" data-basealign="slide"
                         data-responsive_offset="off" data-responsive="off"
                         data-frames='[{"from":"opacity:0;","speed":1500,"to":"o:0;","delay":150,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1500,"to":"opacity:0;","ease":"Power2.easeInOut"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 6;text-transform:left;background-color:rgba(0,0,0,0.5);">
                    </div>

                    <!-- LAYERS 2 number block-->
                    <div class="tp-caption rev-btn  tp-resizeme slider-block sx-bg-primary"
                         id="slide-75-layer-2"
                         data-x="['left','left','left','center']" data-hoffset="['60','60','30','0']"
                         data-y="['middle','middle','middle','top']"
                         data-voffset="['-220','-220','-220','50']"
                         data-fontweight="['600','600','600','600']" data-fontsize="['120','120','80','80']"
                         data-lineheight="['120','120','80','80']" data-height="none"
                         data-whitespace="nowrap"
                         data-type="button" data-responsive_offset="on" data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},
                                {"delay":"wait","speed":500,"to":"y:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power1.easeIn"}]'
                         data-textAlign="['left','left','left','center']" data-paddingtop="[20,20,20,20]"
                         data-paddingright="[10,10,10,10]" data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[10,10,10,10]"
                         style="z-index: 10; font-family: 'Poppins', sans-serif;">
                        03
                    </div>

                    <!-- LAYER 3  Thin text title-->
                    <div class="tp-caption   tp-resizeme slider-tag-line" id="slide-75-layer-3"
                         data-x="['left','left','left','center']" data-hoffset="['60','60','30','0']"
                         data-y="['middle','middle','middle','top']"
                         data-voffset="['-80','-80','-80','170']"
                         data-fontsize="['64','64','60','40']" data-lineheight="['74','74','70','50']"
                         data-width="['700','650','620','380']" data-height="none" data-whitespace="nowrap"
                         data-type="text" data-responsive_offset="on" data-frames='[{"delay":300,"speed":750,"sfxcolor":"#fff","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},
                                {"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoleft","frame":"999","to":"z:0;","ease":"Power4.easeOut"}]'
                         data-textAlign="['left','left','left','center']" data-paddingtop="[10,10,10,10]"
                         data-paddingright="[20,20,20,0]" data-paddingbottom="[10,10,10,10]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 10; font-weight:200; letter-spacing:10px; color: #fff;font-family: 'Poppins', sans-serif; text-transform:uppercase">
                        Creative
                    </div>

                    <!-- LAYER 4  Bold Title-->
                    <div class="tp-caption   tp-resizeme" id="slide-75-layer-4"
                         data-x="['left','left','left','center']" data-hoffset="['60','60','30','0']"
                         data-y="['middle','middle','middle','top']" data-voffset="['10','10','10','230']"
                         data-fontsize="['64','64','60','40']" data-lineheight="['74','74','70','50']"
                         data-width="['700','700','700','700']" data-height="none" data-whitespace="normal"
                         data-type="text" data-responsive_offset="on" data-frames='[{"delay":200,"speed":750,"sfxcolor":"#fff","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},
                                {"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoleft","frame":"999","to":"z:0;","ease":"Power4.easeOut"}]'
                         data-textAlign="['left','left','left','center']" data-paddingtop="[20,20,20,20]"
                         data-paddingright="[20,20,20,10]" data-paddingbottom="[30,30,30,30]"
                         data-paddingleft="[0,0,0,10]"
                         style="z-index: 10; text-transform:uppercase; letter-spacing:10px; white-space: normal;font-weight: 600; color: #fff;font-family: 'Poppins', sans-serif;">
                        Thinkings
                    </div>

                    <!-- LAYER 5  Paragraph-->
                    <div class="tp-caption   tp-resizeme" id="slide-75-layer-5"
                         data-x="['left','left','left','center']" data-hoffset="['60','60','30','0']"
                         data-y="['middle','middle','middle','top']" data-voffset="['90','90','90','300']"
                         data-fontsize="['20','20','20','20']" data-lineheight="['30','30','30','30']"
                         data-width="['600','600','600','380']" data-height="none" data-whitespace="normal"
                         data-type="text" data-responsive_offset="on" data-frames='[{"delay":200,"speed":750,"sfxcolor":"#fff","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},
                                {"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoleft","frame":"999","to":"z:0;","ease":"Power4.easeOut"}]'
                         data-textAlign="['left','left','left','center']" data-paddingtop="[20,20,20,20]"
                         data-paddingright="[20,20,20,20]" data-paddingbottom="[30,30,30,30]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 10; white-space: normal; color: #fff;font-family: 'Poppins', sans-serif;">
                        A
                        multitask profession which creates any land in beautiful creation.
                    </div>

                    <!-- LAYER 6  Read More-->
                    <div class="tp-caption rev-btn  tp-resizeme" id="slide-75-layer-6"
                         data-x="['left','left','left','center']" data-hoffset="['60','60','30','0']"
                         data-y="['middle','middle','middle','top']"
                         data-voffset="['180','180','180','420']"
                         data-width="none" data-height="none" data-whitespace="nowrap" data-type="button"
                         data-responsive_offset="on" data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},
                                {"delay":"wait","speed":500,"to":"y:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power1.easeIn"}]'
                         data-textAlign="['left','left','left','center']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index:9; line-height:30px;"><a href="Javascript:;"
                                                                 class="site-button-secondry btn-half"><span> Read More</span></a>
                    </div>

                    <!-- LAYER 7 left Dark Block -->
                    <div class="tp-caption rev-btn  tp-resizeme rev-slider-white-block"
                         id="slide-75-layer-7"
                         data-x="['right','right','left','right']" data-hoffset="['870','570','0','870']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-width="full" data-height="full" data-whitespace="nowrap" data-type="button"
                         data-responsive_offset="on" data-frames='[{"from":"y:[-0%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":0,"to":"o:1;","delay":0,"ease":"Power3.easeInOut"},
                                {"delay":"wait","speed":0,"to":"y:[-0%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power1.easeIn"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[250,250,250,250]"
                         data-paddingright="[250,150,150,150]" data-paddingbottom="[250,250,250,250]"
                         data-paddingleft="[250,150,250,250]"
                         style="z-index: 6; width:6000px;background-color:#D4AA32;height:100vh;"></div>

                    <!-- Border left Part -->
                    <div class="tp-caption tp-shape tp-shapewrapper " id="slide-75-layer-8"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-width="full" data-height="full" data-whitespace="nowrap"
                         data-visibility="['on','on','off','off']" data-type="shape" data-basealign="slide"
                         data-responsive_offset="off" data-responsive="off"
                         data-frames='[{"delay":50,"speed":100,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power3.easeIn"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index:8;background-color:rgba(0, 0, 0, 0);border-left:40px solid #eef1f2;"></div>

                    <!-- Border bottom Part -->
                    <div class="tp-caption tp-shape tp-shapewrapper " id="slide-75-layer-7"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-width="full" data-height="full" data-whitespace="nowrap"
                         data-visibility="['on','on','off','off']" data-type="shape" data-basealign="slide"
                         data-responsive_offset="off" data-responsive="off"
                         data-frames='[{"delay":50,"speed":100,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power3.easeIn"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index:8;background-color:rgba(0, 0, 0, 0);border-bottom:80px solid #eef1f2;"></div>
                </li>


            </ul>
            <div class="tp-bannertimer"></div>
            <!-- left side social bar-->
            <div class="slide-left-social">
                <ul class="clearfix">
                    <li><a href="#" class="sx-title-swip" data-hover="Linkedin">Linkedin</a></li>
                    <li><a href="#" class="sx-title-swip" data-hover="Twitter">Twitter</a></li>
                    <li><a href="#" class="sx-title-swip" data-hover="Facebook">Facebook</a></li>
                </ul>
            </div>

        </div>
    </div>
    <!-- SLIDER END -->

    <!-- START SECTION POPULAR PLACES (Блок с категориями: Квартиры) -->
    <section class="popular-places home18">
        <div class="container-fluid">
            <div class="section-title">
                <h3>Popular</h3>
                <h2>Квартиры</h2>
            </div>
            <div class="row">
                <div class="col-md-12">
                </div>
                <div class="col-sm-6 col-md-6 col-lg-4 col-xl" data-aos="zoom-in" data-aos-delay="150">
                    <!-- Image Box -->
                    <a href="properties-map.html" class="img-box hover-effect">
                        <img src="images/popular-places/12.jpg" class="img-fluid w100" alt="">
                        <!-- Badge -->
                        <div class="listing-badges">
                            <span class="featured">Featured</span>
                        </div>
                        <div class="img-box-content visible">
                            <h4>New York City </h4>
                            <span>203 Properties</span>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-4 col-xl" data-aos="zoom-in" data-aos-delay="250">
                    <!-- Image Box -->
                    <a href="properties-map.html" class="img-box hover-effect">
                        <img src="images/popular-places/13.jpg" class="img-fluid w100" alt="">
                        <div class="img-box-content visible">
                            <h4>Los Angeles</h4>
                            <span>307 Properties</span>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-4 col-xl" data-aos="zoom-in" data-aos-delay="350">
                    <!-- Image Box -->
                    <a href="properties-map.html" class="img-box hover-effect no-mb">
                        <img src="images/popular-places/14.jpg" class="img-fluid w100" alt="">
                        <div class="img-box-content visible">
                            <h4>San Francisco </h4>
                            <span>409 Properties</span>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-4 col-xl" data-aos="zoom-in" data-aos-delay="450">
                    <!-- Image Box -->
                    <a href="properties-map.html" class="img-box hover-effect no-mb x3">
                        <img src="images/popular-places/15.jpg" class="img-fluid w100" alt="">
                        <!-- Badge -->
                        <div class="listing-badges">
                            <span class="featured">Featured</span>
                        </div>
                        <div class="img-box-content visible">
                            <h4>Miami</h4>
                            <span>507 Properties</span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- END SECTION POPULAR PLACES -->

    <!-- START SECTION POPULAR PLACES (Блок с категориями: Дома) -->
    <section class="popular-places home18">
        <div class="container-fluid">
            <div class="section-title">
                <h3>Popular</h3>
                <h2>Дома</h2>
            </div>
            <div class="row">
                <div class="col-md-12">
                </div>
                <div class="col-sm-6 col-md-6 col-lg-4 col-xl" data-aos="zoom-in" data-aos-delay="150">
                    <!-- Image Box -->
                    <a href="properties-map.html" class="img-box hover-effect">
                        <img src="images/popular-places/12.jpg" class="img-fluid w100" alt="">
                        <!-- Badge -->
                        <div class="listing-badges">
                            <span class="featured">Featured</span>
                        </div>
                        <div class="img-box-content visible">
                            <h4>New York City </h4>
                            <span>203 Properties</span>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-4 col-xl" data-aos="zoom-in" data-aos-delay="250">
                    <!-- Image Box -->
                    <a href="properties-map.html" class="img-box hover-effect">
                        <img src="images/popular-places/13.jpg" class="img-fluid w100" alt="">
                        <div class="img-box-content visible">
                            <h4>Los Angeles</h4>
                            <span>307 Properties</span>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-4 col-xl" data-aos="zoom-in" data-aos-delay="350">
                    <!-- Image Box -->
                    <a href="properties-map.html" class="img-box hover-effect no-mb">
                        <img src="images/popular-places/14.jpg" class="img-fluid w100" alt="">
                        <div class="img-box-content visible">
                            <h4>San Francisco </h4>
                            <span>409 Properties</span>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-4 col-xl" data-aos="zoom-in" data-aos-delay="450">
                    <!-- Image Box -->
                    <a href="properties-map.html" class="img-box hover-effect no-mb x3">
                        <img src="images/popular-places/15.jpg" class="img-fluid w100" alt="">
                        <!-- Badge -->
                        <div class="listing-badges">
                            <span class="featured">Featured</span>
                        </div>
                        <div class="img-box-content visible">
                            <h4>Miami</h4>
                            <span>507 Properties</span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- END SECTION POPULAR PLACES -->

    <!-- START SECTION INFO-HELP (Изображение к Блоку «Почему стоит выбрать нас») -->
    <section class="info-help h18">
        <div class="container">
            <div class="row info-head">
                <div class="col-lg-12 col-md-8 col-xs-8">
                    <div class="info-text" data-aos="fade-up" data-aos-delay="200">
                        <h3 class="text-center mb-0">Почему стоит выбрать нас</h3>
                        <p class="text-center mb-4 p-0">We offer perfect real estate services</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END SECTION INFO-HELP -->

    <!-- START SECTION INFO (Блок «Почему стоит выбрать нас») -->
    <section _ngcontent-bgi-c3="" class="featured-boxes-area bg-white-1">
        <div _ngcontent-bgi-c3="" class="container-fluid">
            <div _ngcontent-bgi-c3="" class="featured-boxes-inner">
                <div _ngcontent-bgi-c3="" class="row m-0">
                    <div _ngcontent-bgi-c3="" class="col-lg-3 col-sm-6 col-md-6 p-0" data-aos="fade-up" data-aos-delay="250">
                        <div _ngcontent-bgi-c3="" class="single-featured-box">
                            <div _ngcontent-bgi-c3="" class="icon color-fb7756"><img src="images/icons/i-1.svg" width="85" height="85" alt=""></div>
                            <h3 _ngcontent-bgi-c3="" class="mt-5">Find Your Home</h3>
                            <p _ngcontent-bgi-c3="">Lorem ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan.</p><a _ngcontent-bgi-c3="" class="read-more-btn" href="single-property-1.html">Read More</a></div>
                    </div>
                    <div _ngcontent-bgi-c3="" class="col-lg-3 col-sm-6 col-md-6 p-0" data-aos="fade-up" data-aos-delay="350">
                        <div _ngcontent-bgi-c3="" class="single-featured-box">
                            <div _ngcontent-bgi-c3="" class="icon color-facd60"><img src="images/icons/i-2.svg" width="85" height="85" alt=""></div>
                            <h3 _ngcontent-bgi-c3="" class="mt-5">Trusted by thousands</h3>
                            <p _ngcontent-bgi-c3="">Lorem ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan.</p><a _ngcontent-bgi-c3="" class="read-more-btn" href="single-property-1.html">Read More</a></div>
                    </div>
                    <div _ngcontent-bgi-c3="" class="col-lg-3 col-sm-6 col-md-6 p-0" data-aos="fade-up" data-aos-delay="450">
                        <div _ngcontent-bgi-c3="" class="single-featured-box">
                            <div _ngcontent-bgi-c3="" class="icon color-1ac0c6"><img src="images/icons/i-3.svg" width="85" height="85" alt=""></div>
                            <h3 _ngcontent-bgi-c3="" class="mt-5">Financing made easy</h3>
                            <p _ngcontent-bgi-c3="">Lorem ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan.</p><a _ngcontent-bgi-c3="" class="read-more-btn" href="single-property-1.html">Read More</a></div>
                    </div>
                    <div _ngcontent-bgi-c3="" class="col-lg-3 col-sm-6 col-md-6 p-0" data-aos="fade-up" data-aos-delay="550">
                        <div _ngcontent-bgi-c3="" class="single-featured-box">
                            <div _ngcontent-bgi-c3="" class="icon"><img src="images/icons/i-4.svg" width="85" height="85" alt=""></div>
                            <h3 _ngcontent-bgi-c3="" class="mt-5">24/7 support</h3>
                            <p _ngcontent-bgi-c3="">Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan.</p><a _ngcontent-bgi-c3="" class="read-more-btn" href="single-property-1.html">Read More</a></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END SECTION INFO -->

    <!-- START SECTION AGENTS (Блок «Знакомьтесь — наши агенты по недвижимости») -->
    <section class="team bg-white-3">
        <div class="container-fluid">
            <div class="section-title col-md-5">
                <h3>Знакомьтесь —</h3>
                <h2><nobr>наши агенты по недвижимости</nobr></h2>
            </div>
            <div class="row team-all">
                <!--Team Block-->
                <div class="team-block col-sm-6 col-md-4 col-lg-4 col-xl-2" data-aos="fade-up" data-aos-delay="150">
                    <div class="inner-box team-details">
                        <div class="image team-head">
                            <a href="agents-listing-grid.html"><img src="images/team/t-5.jpg" alt="" /></a>
                            <div class="team-hover">
                                <ul class="team-social">
                                    <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="lower-box">
                            <h3><a href="agents-listing-grid.html">Carls Jhons</a></h3>
                            <div class="designation">Real Estate Agent</div>
                        </div>
                    </div>
                </div>
                <!--Team Block-->
                <div class="team-block col-sm-6 col-md-4 col-lg-4 col-xl-2" data-aos="fade-up" data-aos-delay="250">
                    <div class="inner-box team-details">
                        <div class="image team-head">
                            <a href="agents-listing-grid.html"><img src="images/team/t-6.jpg" alt="" /></a>
                            <div class="team-hover">
                                <ul class="team-social">
                                    <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="lower-box">
                            <h3><a href="agents-listing-grid.html">Arling Tracy</a></h3>
                            <div class="designation">Real Estate Agent</div>
                        </div>
                    </div>
                </div>
                <!--Team Block-->
                <div class="team-block col-sm-6 col-md-4 col-lg-4 col-xl-2" data-aos="fade-up" data-aos-delay="350">
                    <div class="inner-box team-details">
                        <div class="image team-head">
                            <a href="agents-listing-grid.html"><img src="images/team/t-7.jpg" alt="" /></a>
                            <div class="team-hover">
                                <ul class="team-social">
                                    <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="lower-box">
                            <h3><a href="agents-listing-grid.html">Mark Web</a></h3>
                            <div class="designation">Real Estate Agent</div>
                        </div>
                    </div>
                </div>
                <!--Team Block-->
                <div class="team-block col-sm-6 col-md-4 col-lg-4 col-xl-2 pb-none" data-aos="fade-up" data-aos-delay="450">
                    <div class="inner-box team-details">
                        <div class="image team-head">
                            <a href="agents-listing-grid.html"><img src="images/team/t-8.jpg" alt="" /></a>
                            <div class="team-hover">
                                <ul class="team-social">
                                    <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="lower-box">
                            <h3><a href="agents-listing-grid.html">Katy Grace</a></h3>
                            <div class="designation">Real Estate Agent</div>
                        </div>
                    </div>
                </div>
                <div class="team-block col-sm-6 col-md-4 col-lg-4 col-xl-2 pb-none" data-aos="fade-up" data-aos-delay="550">
                    <div class="inner-box team-details">
                        <div class="image team-head">
                            <a href="agents-listing-grid.html"><img src="images/team/team-image-2.jpg" alt="" /></a>
                            <div class="team-hover">
                                <ul class="team-social">
                                    <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="lower-box">
                            <h3><a href="agents-listing-grid.html">Chris Melo</a></h3>
                            <div class="designation">Real Estate Agent</div>
                        </div>
                    </div>
                </div>
                <div class="team-block col-sm-6 col-md-4 col-lg-4 col-xl-2 pb-none" data-aos="fade-up" data-aos-delay="650">
                    <div class="inner-box team-details">
                        <div class="image team-head">
                            <a href="agents-listing-grid.html"><img src="images/team/team-image-7.jpg" alt="" /></a>
                            <div class="team-hover">
                                <ul class="team-social">
                                    <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="lower-box">
                            <h3><a href="agents-listing-grid.html">Nina Thomas</a></h3>
                            <div class="designation">Real Estate Agent</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END SECTION AGENTS -->

    <!-- START PRELOADER -->
    <div id="preloader">
        <div id="status">
            <div class="status-mes"></div>
        </div>
    </div>
    <!-- END PRELOADER -->

@endsection
