@extends('layout')

@section('title')
    Agencies Details - Connectimobil.md
@endsection

@section('body')
    <div class="inner-pages agents homepage-4 det ag-de hd-white int_white_bg h19 homepage-5 homepage-19">
        <!-- Wrapper -->
        <div id="wrapper" class="int_main_wraapper">
            @endsection

            @section('/body')
        </div>
        <!-- Wrapper / End -->
    </div>
@endsection

@section('main_content')

    <!-- START SECTION AGENTS DETAILS -->
    <section class="blog blog-section portfolio single-proper details mb-0">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <section class="headings-2 pt-0 hee">
                                <div class="pro-wrapper">
                                    <div class="detail-wrapper-body">
                                        <div class="listing-title-bar">
                                            <div class="text-heading text-left">
                                                <p><a href="../index">Home </a> &nbsp;/&nbsp;
                                                    <span>Agencies Single</span>
                                                </p>
                                            </div>
                                            <h3>Agencies Single</h3>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <div class="news-item news-item-sm">
                                <a href="agencies-details" class="news-img-link">
                                    <div class="news-item-img homes">
                                        <div class="homes-tag button alt featured">4 Listings</div>
                                        <img class="resp-img" src="images/partners/ag-6.png" alt="blog image">
                                    </div>
                                </a>
                                <div class="news-item-text">
                                    <a href="agencies-details"><h3>Capital Partners</h3></a>
                                    <div class="the-agents">
                                        <ul class="the-agents-details">
                                            <li><a href="#">Office: (234) 0200 17813</a></li>
                                            <li><a href="#">Mobile: (657) 9854 12095</a></li>
                                            <li><a href="#">Fax: 809 123 0951</a></li>
                                            <li><a href="#">Email: info@agent.com</a></li>
                                        </ul>
                                    </div>
                                    <div class="news-item-bottom">
                                        <a href="properties-full-grid-2" class="news-link">View My Listings</a>
                                        <div class="admin">
                                            <p>Arling Tracy</p>
                                            <img src="images/testimonials/ts-1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="blog-pots py-0">
                        <div class="blog-info details mb-30">

                            <!-- START LISTING PROPERTIES -->
                            <section>
                                <div class="container-px-0">
                                    <h5>Listing</h5>
                                    <div class="row">
                                        <div class="item col-lg-4 col-md-6 col-xs-12 landscapes sale">
                                            <div class="project-single">
                                                <div class="project-inner project-head">
                                                    <div class="homes">
                                                        <!-- homes img -->
                                                        <a href="single-property-3" class="homes-img">
                                                            <div class="homes-tag button alt featured">Featured
                                                            </div>
                                                            <div class="homes-tag button alt sale">For Sale</div>
                                                            <div class="homes-price">$9,000/mo</div>
                                                            <img src="images/blog/b-11.jpg" alt="home-1"
                                                                 class="img-responsive">
                                                        </a>
                                                    </div>
                                                    <div class="button-effect">
                                                        <a href="single-property-3" class="btn"><i
                                                                class="fa fa-link"></i></a>
                                                        <a href="https://www.youtube.com/watch?v=14semTlwyUY"
                                                           class="btn popup-video popup-youtube"><i
                                                                class="fas fa-video"></i></a>
                                                        <a href="single-property-3" class="img-poppu btn"><i
                                                                class="fa fa-photo"></i></a>
                                                    </div>
                                                </div>
                                                <!-- homes content -->
                                                <div class="homes-content">
                                                    <!-- homes address -->
                                                    <h3><a href="single-property-3">Real House Luxury Villa</a></h3>
                                                    <p class="homes-address mb-3">
                                                        <a href="single-property-3">
                                                            <i class="fa fa-map-marker"></i><span>Est St, 77 - Central Park South, NYC</span>
                                                        </a>
                                                    </p>
                                                    <!-- homes List -->
                                                    <ul class="homes-list clearfix">
                                                        <li>
                                                            <span>6 Beds</span>
                                                        </li>
                                                        <li>
                                                            <span>3 Baths</span>
                                                        </li>
                                                        <li>
                                                            <span>720 sq ft</span>
                                                        </li>
                                                        <li>
                                                            <span>2 Garages</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item col-lg-4 col-md-6 col-xs-12 people rent">
                                            <div class="project-single">
                                                <div class="project-inner project-head">
                                                    <div class="homes">
                                                        <!-- homes img -->
                                                        <a href="single-property-3" class="homes-img">
                                                            <div class="homes-tag button sale rent">For Rent</div>
                                                            <div class="homes-price">$3,000/mo</div>
                                                            <img src="images/blog/b-12.jpg" alt="home-1"
                                                                 class="img-responsive">
                                                        </a>
                                                    </div>
                                                    <div class="button-effect">
                                                        <a href="single-property-3" class="btn"><i
                                                                class="fa fa-link"></i></a>
                                                        <a href="https://www.youtube.com/watch?v=14semTlwyUY"
                                                           class="btn popup-video popup-youtube"><i
                                                                class="fas fa-video"></i></a>
                                                        <a href="single-property-2" class="img-poppu btn"><i
                                                                class="fa fa-photo"></i></a>
                                                    </div>
                                                </div>
                                                <!-- homes content -->
                                                <div class="homes-content">
                                                    <!-- homes address -->
                                                    <h3><a href="single-property-3">Real House Luxury Villa</a></h3>
                                                    <p class="homes-address mb-3">
                                                        <a href="single-property-3">
                                                            <i class="fa fa-map-marker"></i><span>Est St, 77 - Central Park South, NYC</span>
                                                        </a>
                                                    </p>
                                                    <!-- homes List -->
                                                    <ul class="homes-list clearfix">
                                                        <li>
                                                            <span>6 Beds</span>
                                                        </li>
                                                        <li>
                                                            <span>3 Baths</span>
                                                        </li>
                                                        <li>
                                                            <span>720 sq ft</span>
                                                        </li>
                                                        <li>
                                                            <span>2 Garages</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item col-lg-4 col-md-6 col-xs-12 people sale no-pb">
                                            <div class="project-single no-mb">
                                                <div class="project-inner project-head">
                                                    <div class="homes">
                                                        <!-- homes img -->
                                                        <a href="single-property-3" class="homes-img">
                                                            <div class="homes-tag button alt sale">For Sale</div>
                                                            <div class="homes-price">$9,000/mo</div>
                                                            <img src="images/feature-properties/fp-11.jpg"
                                                                 alt="home-1" class="img-responsive">
                                                        </a>
                                                    </div>
                                                    <div class="button-effect">
                                                        <a href="single-property-3" class="btn"><i
                                                                class="fa fa-link"></i></a>
                                                        <a href="https://www.youtube.com/watch?v=14semTlwyUY"
                                                           class="btn popup-video popup-youtube"><i
                                                                class="fas fa-video"></i></a>
                                                        <a href="single-property-2" class="img-poppu btn"><i
                                                                class="fa fa-photo"></i></a>
                                                    </div>
                                                </div>
                                                <!-- homes content -->
                                                <div class="homes-content">
                                                    <!-- homes address -->
                                                    <h3><a href="single-property-3">Real House Luxury Villa</a></h3>
                                                    <p class="homes-address mb-3">
                                                        <a href="single-property-3">
                                                            <i class="fa fa-map-marker"></i><span>Est St, 77 - Central Park South, NYC</span>
                                                        </a>
                                                    </p>
                                                    <!-- homes List -->
                                                    <ul class="homes-list clearfix">
                                                        <li>
                                                            <span>6 Beds</span>
                                                        </li>
                                                        <li>
                                                            <span>3 Baths</span>
                                                        </li>
                                                        <li>
                                                            <span>720 sq ft</span>
                                                        </li>
                                                        <li>
                                                            <span>2 Garages</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="item col-lg-4 col-md-6 col-xs-12 people sale no-pb">
                                            <div class="project-single no-mb">
                                                <div class="project-inner project-head">
                                                    <div class="homes">
                                                        <!-- homes img -->
                                                        <a href="single-property-3" class="homes-img">
                                                            <div class="homes-tag button alt sale">For Sale</div>
                                                            <div class="homes-price">$9,000/mo</div>
                                                            <img src="images/feature-properties/fp-11.jpg"
                                                                 alt="home-1" class="img-responsive">
                                                        </a>
                                                    </div>
                                                    <div class="button-effect">
                                                        <a href="single-property-3" class="btn"><i
                                                                class="fa fa-link"></i></a>
                                                        <a href="https://www.youtube.com/watch?v=14semTlwyUY"
                                                           class="btn popup-video popup-youtube"><i
                                                                class="fas fa-video"></i></a>
                                                        <a href="single-property-2" class="img-poppu btn"><i
                                                                class="fa fa-photo"></i></a>
                                                    </div>
                                                </div>
                                                <!-- homes content -->
                                                <div class="homes-content">
                                                    <!-- homes address -->
                                                    <h3><a href="single-property-3">Real House Luxury Villa</a></h3>
                                                    <p class="homes-address mb-3">
                                                        <a href="single-property-3">
                                                            <i class="fa fa-map-marker"></i><span>Est St, 77 - Central Park South, NYC</span>
                                                        </a>
                                                    </p>
                                                    <!-- homes List -->
                                                    <ul class="homes-list clearfix">
                                                        <li>
                                                            <span>6 Beds</span>
                                                        </li>
                                                        <li>
                                                            <span>3 Baths</span>
                                                        </li>
                                                        <li>
                                                            <span>720 sq ft</span>
                                                        </li>
                                                        <li>
                                                            <span>2 Garages</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item col-lg-4 col-md-6 it2 col-xs-12 web rent no-pb x2">
                                            <div class="project-single no-mb last">
                                                <div class="project-inner project-head">
                                                    <div class="homes">
                                                        <!-- homes img -->
                                                        <a href="single-property-3" class="homes-img">
                                                            <div class="homes-tag button alt featured">Featured
                                                            </div>
                                                            <div class="homes-tag button sale rent">For Rent</div>
                                                            <div class="homes-price">$3,000/mo</div>
                                                            <img src="images/feature-properties/fp-12.jpg"
                                                                 alt="home-1" class="img-responsive">
                                                        </a>
                                                    </div>
                                                    <div class="button-effect">
                                                        <a href="single-property-3" class="btn"><i
                                                                class="fa fa-link"></i></a>
                                                        <a href="https://www.youtube.com/watch?v=14semTlwyUY"
                                                           class="btn popup-video popup-youtube"><i
                                                                class="fas fa-video"></i></a>
                                                        <a href="single-property-2" class="img-poppu btn"><i
                                                                class="fa fa-photo"></i></a>
                                                    </div>
                                                </div>
                                                <!-- homes content -->
                                                <div class="homes-content">
                                                    <!-- homes address -->
                                                    <h3><a href="single-property-3">Real House Luxury Villa</a></h3>
                                                    <p class="homes-address mb-3">
                                                        <a href="single-property-3">
                                                            <i class="fa fa-map-marker"></i><span>Est St, 77 - Central Park South, NYC</span>
                                                        </a>
                                                    </p>
                                                    <!-- homes List -->
                                                    <ul class="homes-list clearfix">
                                                        <li>
                                                            <span>6 Beds</span>
                                                        </li>
                                                        <li>
                                                            <span>3 Baths</span>
                                                        </li>
                                                        <li>
                                                            <span>720 sq ft</span>
                                                        </li>
                                                        <li>
                                                            <span>2 Garages</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item col-lg-4 col-md-6 col-xs-12 people rent">
                                            <div class="project-single">
                                                <div class="project-inner project-head">
                                                    <div class="homes">
                                                        <!-- homes img -->
                                                        <a href="single-property-3" class="homes-img">
                                                            <div class="homes-tag button sale rent">For Rent</div>
                                                            <div class="homes-price">$3,000/mo</div>
                                                            <img src="images/blog/b-12.jpg" alt="home-1"
                                                                 class="img-responsive">
                                                        </a>
                                                    </div>
                                                    <div class="button-effect">
                                                        <a href="single-property-3" class="btn"><i
                                                                class="fa fa-link"></i></a>
                                                        <a href="https://www.youtube.com/watch?v=14semTlwyUY"
                                                           class="btn popup-video popup-youtube"><i
                                                                class="fas fa-video"></i></a>
                                                        <a href="single-property-2" class="img-poppu btn"><i
                                                                class="fa fa-photo"></i></a>
                                                    </div>
                                                </div>
                                                <!-- homes content -->
                                                <div class="homes-content">
                                                    <!-- homes address -->
                                                    <h3><a href="single-property-3">Real House Luxury Villa</a></h3>
                                                    <p class="homes-address mb-3">
                                                        <a href="single-property-3">
                                                            <i class="fa fa-map-marker"></i><span>Est St, 77 - Central Park South, NYC</span>
                                                        </a>
                                                    </p>
                                                    <!-- homes List -->
                                                    <ul class="homes-list clearfix">
                                                        <li>
                                                            <span>6 Beds</span>
                                                        </li>
                                                        <li>
                                                            <span>3 Baths</span>
                                                        </li>
                                                        <li>
                                                            <span>720 sq ft</span>
                                                        </li>
                                                        <li>
                                                            <span>2 Garages</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <!-- END LISTING PROPERTIES -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END SECTION AGENTS DETAILS -->

@endsection


