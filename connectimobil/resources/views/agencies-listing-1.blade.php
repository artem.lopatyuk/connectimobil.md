@extends('layout')

@section('title')
    Agencies - Connectimobil.md
@endsection

@section('body')
    <div class="inner-pages agents homepage-4 hd-white int_white_bg h19 homepage-5 homepage-19">
        <!-- Wrapper -->
        <div id="wrapper" class="int_main_wraapper">
            @endsection

            @section('/body')
        </div>
        <!-- Wrapper / End -->
    </div>
@endsection

@section('main_content')

    <!-- START SECTION PROPERTIES LISTING -->
    <section class="properties-list featured portfolio blog">
        <div class="container">
            <section class="headings-2 pt-0 pb-55">
                <div class="pro-wrapper">
                    <div class="detail-wrapper-body">
                        <div class="listing-title-bar">
                            <div class="text-heading text-left">
                                <p class="pb-2"><a href="index">Home </a> &nbsp;/&nbsp; <span>Listings</span></p>
                            </div>
                            <h3>Our Agencies</h3>
                        </div>
                    </div>
                </div>
            </section>
            <section class="headings-2 pt-0">
                <div class="pro-wrapper">
                    <div class="detail-wrapper-body">
                        <div class="listing-title-bar">
                            <div class="text-heading text-left">
                                <p class="font-weight-bold mb-0 mt-3">8 Search results</p>
                            </div>
                        </div>
                    </div>
                    <div
                        class="cod-pad single detail-wrapper mr-2 mt-0 d-flex justify-content-md-end align-items-center">
                        <div class="input-group border rounded input-group-lg w-auto mr-4">
                            <label
                                class="input-group-text bg-transparent border-0 text-uppercase letter-spacing-093 pr-1 pl-3"
                                for="inputGroupSelect01"><i
                                    class="fas fa-align-left fs-16 pr-2"></i>Sortby:</label>
                            <select
                                class="form-control border-0 bg-transparent shadow-none p-0 selectpicker sortby"
                                data-style="bg-transparent border-0 font-weight-600 btn-lg pl-0 pr-3"
                                id="inputGroupSelect01" name="sortby">
                                <option selected>Top Selling</option>
                                <option value="1">Most Viewed</option>
                                <option value="2">Price(low to high)</option>
                                <option value="3">Price(high to low)</option>
                            </select>
                        </div>
                        <div class="sorting-options">
                            <a href="properties-full-list-1" class="change-view-btn lde"><i
                                    class="fa fa-th-list"></i></a>
                            <a href="#" class="change-view-btn active-view-btn"><i
                                    class="fa fa-th-large"></i></a>
                        </div>
                    </div>
                </div>
            </section>
                <div class="row">
                    <div class="item col-lg-4 col-md-6 col-xs-12 landscapes sale">
                        <div class="project-single">
                            <div class="project-inner project-head">
                                <div class="homes">
                                    <!-- homes img -->
                                    <a href="agencies-details" class="homes-img">
                                        <div class="homes-tag button alt featured">3 Listings</div>
                                        <img src="images/partners/ag-1.png" alt="home-1" class="img-responsive">
                                    </a>
                                </div>
                            </div>
                            <!-- homes content -->
                            <div class="homes-content">
                                <!-- homes address -->
                                <div class="the-agents">
                                    <h3><a href="agencies-details">Capital Partners</a></h3>
                                    <ul class="the-agents-details">
                                        <li><a href="#">Office: (234) 0200 17813</a></li>
                                        <li><a href="#">Mobile: (657) 9854 12095</a></li>
                                        <li><a href="#">Fax: 809 123 0951</a></li>
                                        <li><a href="#">Email: info@agent.com</a></li>
                                    </ul>
                                </div>
                                <div class="footer">
                                    <a href="agencies-details">
                                        <img src="images/testimonials/ts-1.jpg" alt="" class="mr-2"> Arling Tracy
                                    </a>
                                    <span class="view-my-listing">
                                                <a href="properties-full-grid-1">Посмотреть объекты</a>
                                            </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item col-lg-4 col-md-6 col-xs-12 people rent">
                        <div class="project-single">
                            <div class="project-inner project-head">
                                <div class="homes">
                                    <!-- homes img -->
                                    <a href="agencies-details" class="homes-img">
                                        <div class="homes-tag button alt featured">3 Listings</div>
                                        <img src="images/partners/ag-2.png" alt="home-1" class="img-responsive">
                                    </a>
                                </div>
                            </div>
                            <!-- homes content -->
                            <div class="homes-content">
                                <!-- homes address -->
                                <div class="the-agents">
                                    <h3><a href="agencies-details">Legacy park</a></h3>
                                    <ul class="the-agents-details">
                                        <li><a href="#">Office: (234) 0200 17813</a></li>
                                        <li><a href="#">Mobile: (657) 9854 12095</a></li>
                                        <li><a href="#">Fax: 809 123 0951</a></li>
                                        <li><a href="#">Email: info@agent.com</a></li>
                                    </ul>
                                </div>
                                <div class="footer">
                                    <a href="agencies-details">
                                        <img src="images/testimonials/ts-2.jpg" alt="" class="mr-2"> Carls Jhons
                                    </a>
                                    <span class="view-my-listing">
                                                 <a href="properties-full-grid-1">Посмотреть объекты</a>
                                            </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item col-lg-4 col-md-6 it2 col-xs-12 web rent no-pb x2">
                        <div class="project-single no-mb last">
                            <div class="project-inner project-head">
                                <div class="homes">
                                    <!-- homes img -->
                                    <a href="agencies-details" class="homes-img">
                                        <div class="homes-tag button alt featured">3 Listings</div>
                                        <img src="images/partners/ag-8.png" alt="home-1" class="img-responsive">
                                    </a>
                                </div>
                            </div>
                            <!-- homes content -->
                            <div class="homes-content">
                                <!-- homes address -->
                                <div class="the-agents">
                                    <h3><a href="agencies-details">Lou Mortgage</a></h3>
                                    <ul class="the-agents-details">
                                        <li><a href="#">Office: (234) 0200 17813</a></li>
                                        <li><a href="#">Mobile: (657) 9854 12095</a></li>
                                        <li><a href="#">Fax: 809 123 0951</a></li>
                                        <li><a href="#">Email: info@agent.com</a></li>
                                    </ul>
                                </div>
                                <div class="footer">
                                    <a href="agencies-details">
                                        <img src="images/testimonials/ts-2.jpg" alt="" class="mr-2"> Pedro Lopez
                                    </a>
                                    <span class="view-my-listing">
                                                 <a href="properties-full-grid-1">Посмотреть объекты</a>
                                            </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="item col-lg-4 col-md-6 col-xs-12 people landscapes sale">
                        <div class="project-single">
                            <div class="project-inner project-head">
                                <div class="homes">
                                    <!-- homes img -->
                                    <a href="agencies-details" class="homes-img">
                                        <div class="homes-tag button alt featured">3 Listings</div>
                                        <img src="images/partners/ag-3.png" alt="home-1" class="img-responsive">
                                    </a>
                                </div>
                            </div>
                            <!-- homes content -->
                            <div class="homes-content">
                                <!-- homes address -->
                                <div class="the-agents">
                                    <h3><a href="agencies-details">Live Property</a></h3>
                                    <ul class="the-agents-details">
                                        <li><a href="#">Office: (234) 0200 17813</a></li>
                                        <li><a href="#">Mobile: (657) 9854 12095</a></li>
                                        <li><a href="#">Fax: 809 123 0951</a></li>
                                        <li><a href="#">Email: info@agent.com</a></li>
                                    </ul>
                                </div>
                                <div class="footer">
                                    <a href="agencies-details">
                                        <img src="images/testimonials/ts-3.jpg" alt="" class="mr-2"> Katy Grace
                                    </a>
                                    <span class="view-my-listing">
                                                 <a href="properties-full-grid-1">Посмотреть объекты</a>
                                            </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item col-lg-4 col-md-6 col-xs-12 people landscapes rent">
                        <div class="project-single">
                            <div class="project-inner project-head">
                                <div class="homes">
                                    <!-- homes img -->
                                    <a href="agencies-details" class="homes-img">
                                        <div class="homes-tag button alt featured">3 Listings</div>
                                        <img src="images/partners/ag-4.png" alt="home-1" class="img-responsive">
                                    </a>
                                </div>
                            </div>
                            <!-- homes content -->
                            <div class="homes-content">
                                <!-- homes address -->
                                <div class="the-agents">
                                    <h3><a href="agencies-details">Real Property</a></h3>
                                    <ul class="the-agents-details">
                                        <li><a href="#">Office: (234) 0200 17813</a></li>
                                        <li><a href="#">Mobile: (657) 9854 12095</a></li>
                                        <li><a href="#">Fax: 809 123 0951</a></li>
                                        <li><a href="#">Email: info@agent.com</a></li>
                                    </ul>
                                </div>
                                <div class="footer">
                                    <a href="agencies-details">
                                        <img src="images/testimonials/ts-4.jpg" alt="" class="mr-2"> Mark Web
                                    </a>
                                    <span class="view-my-listing">
                                                 <a href="properties-full-grid-1">Посмотреть объекты</a>
                                            </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item col-lg-4 col-md-6 col-xs-12 people sale no-pb">
                        <div class="project-single no-mb">
                            <div class="project-inner project-head">
                                <div class="homes">
                                    <!-- homes img -->
                                    <a href="agencies-details" class="homes-img">
                                        <div class="homes-tag button alt featured">3 Listings</div>
                                        <img src="images/partners/ag-7.png" alt="home-1" class="img-responsive">
                                    </a>
                                </div>
                            </div>
                            <!-- homes content -->
                            <div class="homes-content">
                                <!-- homes address -->
                                <div class="the-agents">
                                    <h3><a href="agencies-details">True Home</a></h3>
                                    <ul class="the-agents-details">
                                        <li><a href="#">Office: (234) 0200 17813</a></li>
                                        <li><a href="#">Mobile: (657) 9854 12095</a></li>
                                        <li><a href="#">Fax: 809 123 0951</a></li>
                                        <li><a href="#">Email: info@agent.com</a></li>
                                    </ul>
                                </div>
                                <div class="footer">
                                    <a href="agencies-details">
                                        <img src="images/testimonials/ts-1.jpg" alt="" class="mr-2"> Ruth Thomas
                                    </a>
                                    <span class="view-my-listing">
                                                 <a href="properties-full-grid-1">Посмотреть объекты</a>
                                            </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="item col-lg-4 col-md-6 col-xs-12 people landscapes sale">
                        <div class="project-single">
                            <div class="project-inner project-head">
                                <div class="homes">
                                    <!-- homes img -->
                                    <a href="agencies-details" class="homes-img">
                                        <div class="homes-tag button alt featured">3 Listings</div>
                                        <img src="images/partners/ag-5.png" alt="home-1" class="img-responsive">
                                    </a>
                                </div>
                            </div>
                            <!-- homes content -->
                            <div class="homes-content">
                                <!-- homes address -->
                                <div class="the-agents">
                                    <h3><a href="agencies-details">Mojo Homes</a></h3>
                                    <ul class="the-agents-details">
                                        <li><a href="#">Office: (234) 0200 17813</a></li>
                                        <li><a href="#">Mobile: (657) 9854 12095</a></li>
                                        <li><a href="#">Fax: 809 123 0951</a></li>
                                        <li><a href="#">Email: info@agent.com</a></li>
                                    </ul>
                                </div>
                                <div class="footer">
                                    <a href="agencies-details">
                                        <img src="images/testimonials/ts-5.jpg" alt="" class="mr-2"> Nina Thomas
                                    </a>
                                    <span class="view-my-listing">
                                                  <a href="properties-full-grid-1">Посмотреть объекты</a>
                                            </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item col-lg-4 col-md-6 col-xs-12 people landscapes rent">
                        <div class="project-single">
                            <div class="project-inner project-head">
                                <div class="homes">
                                    <!-- homes img -->
                                    <a href="agencies-details" class="homes-img">
                                        <div class="homes-tag button alt featured">3 Listings</div>
                                        <img src="images/partners/ag-6.png" alt="home-1" class="img-responsive">
                                    </a>
                                </div>
                            </div>
                            <!-- homes content -->
                            <div class="homes-content">
                                <!-- homes address -->
                                <div class="the-agents">
                                    <h3><a href="agencies-details">Sweet Houses</a></h3>
                                    <ul class="the-agents-details">
                                        <li><a href="#">Office: (234) 0200 17813</a></li>
                                        <li><a href="#">Mobile: (657) 9854 12095</a></li>
                                        <li><a href="#">Fax: 809 123 0951</a></li>
                                        <li><a href="#">Email: info@agent.com</a></li>
                                    </ul>
                                </div>
                                <div class="footer">
                                    <a href="agencies-details">
                                        <img src="images/testimonials/ts-6.jpg" alt="" class="mr-2"> Ichiro Lee
                                    </a>
                                    <span class="view-my-listing">
                                                  <a href="properties-full-grid-1">Посмотреть объекты</a>
                                            </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END SECTION PROPERTIES LISTING -->

@endsection
