<!-- START FOOTER -->
<footer class="first-footer">
    <div class="top-footer bg-white">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="netabout">
                        <a href="index" class="">
                            <br><br><br>
                            <img src="images/logo-black.svg" alt="netcom">
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="navigation">
                        <h3>Навигация</h3>
                        <div class="nav-footer">
                            <ul>
                                <li><a href="properties-full-grid-1">Properties List</a></li>
                                <li><a href="single-property-3">Property Details</a></li>
                                <li class="no-mgb"><a href="agencies-listing-1">Agents Listing</a>
                                </li>
                            </ul>
                            <ul class="nav-right">
                                <li><a href="agencies-details">Agents Details</a></li>
                                <li><a href="about">About Us</a></li>
                                <li class="no-mgb"><a href="contact-us">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="contactus">
                        <h3>Контакты</h3>
                        <ul>
                            <li>
                                <div class="info">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <p class="in-p">95 South Park Avenue, USA</p>
                                </div>
                            </li>
                            <li>
                                <div class="info">
                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                    <p class="in-p">+456 875 369 208</p>
                                </div>
                            </li>
                            <li>
                                <div class="info">
                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                    <p class="in-p">+456 875 369 208</p>
                                </div>
                            </li>
                            <li>
                                <div class="info">
                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                    <p class="in-p ti">support@findhouses.com</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="second-footer bg-white-3">
        <div class="container">
            <p>2021 © Copyright - All Rights Reserved.</p>
        </div>
    </div>
</footer>

<a data-scroll href="#wrapper" class="go-up"><i class="fa fa-angle-double-up" aria-hidden="true"></i></a>
<!-- END FOOTER -->
